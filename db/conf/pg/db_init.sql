CREATE SCHEMA common;

CREATE TABLE common.service_request (
  case_summary            VARCHAR (255),
  case_status             VARCHAR(51),
  case_source             VARCHAR(51),
  case_created_date       VARCHAR(51),
  case_created_dttm       VARCHAR(51),
  case_closed_date        VARCHAR(51),
  case_closed_dttm        VARCHAR(51),
  first_call_resolution   CHAR,
  customer_zip_code       VARCHAR(51),
  incident_address_1      VARCHAR(255),
  incident_address_2      VARCHAR(255),
  incident_intersection_1 VARCHAR(255),
  incident_intersection_2 VARCHAR(255),
  incident_zip_code       VARCHAR(51),
  geo_lon                 NUMERIC,
  geo_lat                 NUMERIC,
  agency                  VARCHAR(51),
  division                VARCHAR(51),
  major_area              VARCHAR(51),
  type                    VARCHAR(51),
  topic                   VARCHAR(51),
  council_district        VARCHAR(51),
  police_district         VARCHAR(51),
  neighborhood_id         VARCHAR(51),
  UNIQUE (case_summary, case_source, case_created_dttm)
);

CREATE TABLE common.traffic_accidents (
  id                     BIGINT UNIQUE NOT NULL,
  offense_id             BIGINT,
  offense_code           INTEGER,
  offense_code_extension INTEGER,
  offense_type_id        VARCHAR(51),
  offence_category       VARCHAR(255),
  first_occurrence       VARCHAR(51),
  last_occurrence        VARCHAR(51),
  report_date            VARCHAR(51),
  incident_address       VARCHAR(255),
  geo_x                  INTEGER,
  geo_y                  INTEGER,
  geo_lon                NUMERIC,
  geo_lat                NUMERIC,
  district_id            NUMERIC,
  precinct_id            NUMERIC,
  neighborhood_id        VARCHAR(51),
  bicycle_ind            VARCHAR(51),
  pedestrian_ind         VARCHAR(51)
);

import requests
import datetime
import pendulum
import models

to_utc_from_gmt_offset = lambda dt: pendulum.datetime(dt.year, dt.month, dt.day, dt.hour, dt.minute, tz='GMT').in_timezone('UTC')
to_utc_from_utc_offset = lambda dt: pendulum.datetime(dt.year, dt.month, dt.day, dt.hour, dt.minute, tz='UTC')


class File(object):
    name = ""
    path = ""

    def __init__(self, name, path):
        self.name = name
        self.path = path


class Download(object):
    name = ""
    external_url = ""
    destination = ""
    force = False
    file = File
    local_path = ""

    def __init__(self, name, base_model: models.DataSet, force=None):
        self.name = name
        self.external_url = base_model.external
        self.force = self.force if force is None else force
        self.local_path = base_model.local
        self.file = File(self.name, self.local_path)

    def has_update(self):
        resp = requests.head(self.external_url)
        file_time = self.get_timestamp_from_header(resp.headers['last-modified'])
        current_time = to_utc_from_utc_offset(datetime.datetime.now(datetime.timezone.utc))
        diff: int = (current_time-file_time).in_minutes()

        return diff < 11

    def get_timestamp_from_header(self, tsh: str):
        return to_utc_from_gmt_offset(datetime.datetime.strptime(tsh, "%a, %d %b %Y %H:%M:%S %Z"))

    def csv_request(self):
        if self.force or self.has_update():
            return self.name
        else:
            return None


class CSV(object):
    delimiter = ","
    null_values = ["None"]

import database


class Test(object):
    name=""
    sql=""
    result_col=""
    spec_result=""

    def __init__(self, name, sql, spec_result, result_col):
        self.name = name
        self.sql = sql
        self.result_col = result_col
        self.spec_result = spec_result


def run(test:Test):
    db = database.get_instance(True)
    connection = db.connect()

    print("%s:" % test.name)
    result = connection.execute(test.sql)

    for row in result:
        print("result: " + dict(row)[test.result_col])
        print("expected: " + test.spec_result)
        print("passed:")
        print(dict(row)[test.result_col] == test.spec_result)

    connection.close()


def check_all():
    for test in tests:
        run(test)


tests = [
    Test(
        name='traffic_accidents_last_occurrence',
        result_col='last_occurrence',
        sql='SELECT traffic_accidents.last_occurrence FROM %s.traffic_accidents WHERE id = 2013227524;' % database.SCHEMA_NAME,
        spec_result="2013-05-30 17:22:00"
    ),
    Test(
        name='service_request_case_status',
        result_col='case_status',
        sql="SELECT service_request.case_status FROM %s.service_request WHERE case_summary = 'recycle cart' AND case_source = 'Phone' AND case_created_dttm = '12/31/2015 11:26:39 PM';" % database.SCHEMA_NAME,
        spec_result="Closed - Service Completed"
    ),
]

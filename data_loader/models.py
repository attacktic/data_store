import pathlib

import csv_structures.service_requests as SR
import csv_structures.traffic_accidents as TA

current_file = pathlib.Path(__file__)
current_dir = current_file.parent


class Struct(object):
    columns = ""
    key_columns = ""
    upsert_columns = ""
    raw = ""
    col_index = False

    def __init__(self, columns, key_columns, upsert_columns, raw, col_index):
        self.columns = columns
        self.key_columns = key_columns
        self.upsert_columns = upsert_columns
        self.raw = raw
        self.col_index = col_index


class DataSet(object):
    name = ""
    external = ""
    local = ""
    struct = Struct

    def __init__(self, name, external, struct, test=False):
        self.name = name
        self.external = external
        self.local = str(current_dir) + 'raw/' + name + '.csv' if not test else str(current_dir) + test
        self.struct = struct


service_request = DataSet(
    name='service_request',
    external='https://www.denvergov.org/media/gis/DataCatalog/311_service_requests/csv/311_service_requests.csv',
    struct=Struct(SR.COLUMNS, SR.KEY_COLUMNS, SR.UPSERT_COLUMNS, SR.PD_RAW, col_index=False)
)

traffic_accidents = DataSet(
    name='traffic_accidents',
    external='https://www.denvergov.org/media/gis/DataCatalog/traffic_accidents/csv/traffic_accidents.csv',
    struct=Struct(TA.COLUMNS, TA.KEY_COLUMNS, TA.UPSERT_COLUMNS, TA.PD_RAW, col_index=True)
)

data_sets = dict(
    traffic_accidents=traffic_accidents,
    service_request=service_request
)

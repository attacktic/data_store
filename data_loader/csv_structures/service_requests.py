PD_RAW = {
    'case_summary': str,
    'case_status': str,
    'case_source': str,
    'case_created_date': str,
    'case_created_dttm': str,
    'case_closed_date': str,
    'case_closed_dttm': str,
    'first_call_resolution': str,
    'customer_zip_code': str,
    'incident_address_1': str,
    'incident_address_2': str,
    'incident_intersection_1': str,
    'incident_intersection_2': str,
    'incident_zip_code': str,
    'geo_lon': float,
    'geo_lat': float,
    'agency': str,
    'division': str,
    'major_area': str,
    'type': str,
    'topic': str,
    'council_district': str,
    'police_district': str,
    'neighborhood_id': str
}

KEY_COLUMNS = [
    'case_summary',
    'case_source',
    'case_created_dttm'
]

UPSERT_COLUMNS = [
    'case_status',
    'case_closed_date',
    'case_closed_dttm',
    'first_call_resolution',
    'customer_zip_code',
    'incident_address_1',
    'incident_address_2',
    'incident_intersection_1',
    'incident_intersection_2',
    'incident_zip_code',
    'geo_lon',
    'geo_lat',
    'agency',
    'division',
    'major_area',
    'type',
    'topic',
    'council_district',
    'police_district',
    'neighborhood_id'
]

COLUMNS = [
    'case_summary',
    'case_status',
    'case_source',
    'case_created_date',
    'case_created_dttm',
    'case_closed_date',
    'case_closed_dttm',
    'first_call_resolution',
    'customer_zip_code',
    'incident_address_1',
    'incident_address_2',
    'incident_intersection_1',
    'incident_intersection_2',
    'incident_zip_code',
    'geo_lon',
    'geo_lat',
    'agency',
    'division',
    'major_area',
    'type',
    'topic',
    'council_district',
    'police_district',
    'neighborhood_id'
]


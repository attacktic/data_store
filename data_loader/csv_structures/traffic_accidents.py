PD_RAW = {
    'id': int,
    'offense_id': int,
    'offense_code': int,
    'offense_code_extension': int,
    'offense_type_id': str,
    'offence_category': str,
    'first_occurrence': str,
    'last_occurrence': str,
    'report_date': str,
    'incident_address': str,
    'geo_x': int,
    'geo_y': int,
    'geo_lon': float,
    'geo_lat': float,
    'district_id': float,
    'precinct_id': float,
    'neighborhood_id': str,
    'bicycle_ind': str,
    'pedestrian_ind': str
}

KEY_COLUMNS = [
    'id'
]

UPSERT_COLUMNS = [
    'offense_id',
    'offense_code',
    'offense_code_extension',
    'offense_type_id',
    'offence_category',
    'first_occurrence',
    'last_occurrence',
    'report_date',
    'incident_address',
    'geo_x',
    'geo_y',
    'geo_lon',
    'geo_lat',
    'district_id',
    'precinct_id',
    'neighborhood_id',
    'bicycle_ind',
    'pedestrian_ind'
]

COLUMNS = [
    'id',
    'offense_id',
    'offense_code',
    'offense_code_extension',
    'offense_type_id',
    'offence_category',
    'first_occurrence',
    'last_occurrence',
    'report_date',
    'incident_address',
    'geo_x',
    'geo_y',
    'geo_lon',
    'geo_lat',
    'district_id',
    'precinct_id',
    'neighborhood_id',
    'bicycle_ind',
    'pedestrian_ind'
]

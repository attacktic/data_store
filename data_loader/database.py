from sqlalchemy import create_engine
import os

user = os.environ['POSTGRES_USER']
pw = os.environ['POSTGRES_PASSWORD']
db = os.environ['POSTGRES_DB']

SCHEMA_NAME = 'common'
ENCODING = 'ISO-8859-1'


def upsert_cols_builder(update_cols):
    set_stmt_lines = []
    for col in update_cols:
        set_stmt_lines.append("%s = excluded.%s" % (col, col))
    return ", ".join(set_stmt_lines)


def upsert_builder(table, all_cols, key_cols, upsert_columns):
    temp_table = '%s.temp_%s' % (SCHEMA_NAME, table)
    key_cols_str = ", ".join(key_cols)
    all_cols_str = ", ".join(all_cols)
    set_stmt = upsert_cols_builder(upsert_columns)

    stmt = """
        INSERT INTO {schema}.{table} ({all_cols_str})
        SELECT DISTINCT ON ({key_cols_str}) *
        FROM   {temp_table}
        ON     CONFLICT ({key_cols_str}) DO UPDATE
        SET    {set_stmt};
    """.format(schema=SCHEMA_NAME,
               table=table,
               all_cols_str=all_cols_str,
               temp_table=temp_table,
               key_cols_str=key_cols_str,
               set_stmt=set_stmt)
    print(stmt)
    return stmt


def clean_df(df):
    return df.apply(lambda ss: ss.str.strip() if ss.dtype == "str" else ss)


def drop_temp_table_stmt(table):
    return 'DROP TABLE IF EXISTS %s.temp_%s;' % (SCHEMA_NAME, table)


def get_instance(debug):
    i = create_engine('postgresql+psycopg2://%s:%s@db:5432/%s' % (user, pw, db), echo=debug)
    return i

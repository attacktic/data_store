from argparse import ArgumentParser

import pandas as pd
from models import data_sets, DataSet
import logging
from utils import CSV
import database


def load_csv(data_set: DataSet, local_only):
    chunksize = 10 ** 6
    result_sets = []

    logger = get_logger()
    logger.log(logging.INFO, "%s: Update found or forced, download in progress..." % data_set.name)

    hd = data_set.local if local_only else data_set.external

    try:
        for chunk in pd.read_csv(
                hd,
                sep=CSV.delimiter,
                iterator=True,
                error_bad_lines=False,
                encoding=database.ENCODING,
                chunksize=chunksize,
                low_memory=False,
                names=data_set.struct.columns,
                skiprows=2,
                na_values=CSV.null_values,
                keep_default_na=True,
                dtype=data_set.struct.raw):

            result_sets.append(chunk)

        df = database.clean_df(pd.concat(result_sets))

        logger.log(logging.INFO, "%s: Download completed. Dataframe size:%d." % (data_set.name, len(df)))

        db = database.get_instance(local_only)

        logger.log(logging.INFO, "%s: Inserting into temp." % data_set.name)

        df.to_sql(
            "temp_%s" % data_set.name,
            db,
            if_exists='replace',
            chunksize=10000,
            schema=database.SCHEMA_NAME,
            index=False)

        logger.log(logging.INFO, "%s: Upserting prod records." % data_set.name)

        connection = db.connect()
        connection.execute(database.upsert_builder(
            table=data_set.name,
            all_cols=data_set.struct.columns,
            key_cols=data_set.struct.key_columns,
            upsert_columns=data_set.struct.upsert_columns))

        # Drop temp table
        db.execute(database.drop_temp_table_stmt(
            table=data_set.name))

        connection.close()

    except Exception as e:
        # TODO: retry
        print(e)
        logger.log(logging.INFO, e)

        # Remove temp table
        db = database.get_instance()
        db.execute(database.drop_temp_table_stmt(
            table=data_set.name))



def get_logger():
    logger = logging.getLogger()
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    fh = logging.FileHandler('/var/log/application.log')
    fh.setFormatter(formatter)
    logger.setLevel(logging.INFO)
    logger.addHandler(fh)
    return logger


if __name__ == "__main__":
    parser = ArgumentParser(description='load_csv')
    parser.add_argument('--structure', required=True, type=str,
                        help="structure name")
    args = parser.parse_args()
    structure_name = args.structure

    load_csv(data_sets[structure_name], True)


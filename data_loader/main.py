from utils import Download
from argparse import ArgumentParser
import models
import load_data
from models import data_sets, DataSet


parser = ArgumentParser()
parser.add_argument("-f", "--force", dest="force", action="store_true", help="force update", required=False)
parser.add_argument("-l", "--local_only", dest="local_only", action="store_true", help="use local files only", required=False)
args = parser.parse_args()

# Force when local_only is set
force = args.local_only if args.force is False else args.force

service_request_download = Download(
    name='service_request',
    base_model=models.service_request,
    force=force
)

traffic_accidents_download = Download(
    name='traffic_accidents',
    base_model=models.traffic_accidents,
    force=force
)

updated_files = filter(
    None, [
        service_request_download.csv_request(),
        traffic_accidents_download.csv_request()
    ])

for structure_name in updated_files:
    load_data.load_csv(
        data_sets[structure_name],
        args.local_only)

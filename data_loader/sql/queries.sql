-- seasonal trends
SELECT
  case_summary,
  CASE
  WHEN month >= 3 AND month <= 5
    THEN 'spring'
  WHEN month >= 6 AND month <= 8
    THEN 'summer'
  WHEN month >= 9 AND month <= 11
    THEN 'fall'
  ELSE 'winter'
  END AS season,
  SUM(count)
FROM
  (SELECT
     case_summary,
     EXTRACT(MONTH FROM TO_DATE("case_created_date", 'MM/DD/YYYY')) AS month,
     COUNT(1)                                                       AS count
   FROM common.service_request
   GROUP BY 1, 2) AS ST_month
GROUP BY 1, 2
ORDER BY 2, 1;


SELECT
  offense_type_id,
  CASE
  WHEN month >= 3 AND month <= 5
    THEN 'spring'
  WHEN month >= 6 AND month <= 8
    THEN 'summer'
  WHEN month >= 9 AND month <= 11
    THEN 'fall'
  ELSE 'winter'
  END AS season,
  SUM(count)
FROM
  (SELECT
     offense_type_id,
     EXTRACT(MONTH FROM to_timestamp("report_date", 'YYYY-MM-DD HH24:MI:SS')) AS month,
     COUNT(1)
   FROM common.traffic_accidents
   GROUP BY 1, 2) AS TA_month
GROUP BY 1, 2;

-- event types by area

SELECT
  offense_type_id,
  neighborhood_id,
  COUNT(1)
FROM common.traffic_accidents
WHERE neighborhood_id IS NOT NULL
GROUP BY 1, 2
ORDER BY 3 DESC;

SELECT
  common.service_request.case_summary,
  common.traffic_accidents.neighborhood_id,
  COUNT(1)
FROM common.service_request
  JOIN common.traffic_accidents
    ON common.traffic_accidents.geo_lon = common.service_request.geo_lon
       AND common.traffic_accidents.geo_lat = common.service_request.geo_lat
GROUP BY 1, 2
ORDER BY 3 DESC;

-- response times by x,y,z

SELECT
  case_summary,
  MIN(response_time),
  MAX(response_time),
  AVG(response_time)
FROM
  (SELECT
     TO_DATE("case_closed_date", 'MM/DD/YYYY') - TO_DATE("case_created_date", 'MM/DD/YYYY') AS response_time,
     case_summary
   FROM common.service_request
   WHERE case_closed_date IS NOT NULL AND case_created_date IS NOT NULL
   GROUP BY 2, case_closed_date, case_created_date) AS RT
GROUP BY 1
ORDER BY 4 DESC;

SELECT
  type,
  agency,
  MIN(response_time),
  MAX(response_time),
  AVG(response_time)
FROM
  (SELECT
     TO_DATE("case_closed_date", 'MM/DD/YYYY') - TO_DATE("case_created_date", 'MM/DD/YYYY') AS response_time,
     type,
     agency
   FROM common.service_request
   WHERE case_closed_date IS NOT NULL AND case_created_date IS NOT NULL AND type IS NOT NULL AND agency IS NOT NULL
   GROUP BY 2, 3, case_closed_date, case_created_date) AS RT
GROUP BY 1, 2
ORDER BY 5 DESC;

SELECT
  customer_zip_code,
  MIN(response_time),
  MAX(response_time),
  AVG(response_time)
FROM
  (SELECT
     TO_DATE("case_closed_date", 'MM/DD/YYYY') - TO_DATE("case_created_date", 'MM/DD/YYYY') AS response_time,
     customer_zip_code
   FROM common.service_request
   WHERE case_closed_date IS NOT NULL AND case_created_date IS NOT NULL AND customer_zip_code IS NOT NULL
   GROUP BY 2, case_closed_date, case_created_date) AS RT
GROUP BY 1
ORDER BY 4 DESC;
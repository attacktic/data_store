import models
import utils
import load_data
import csv_structures.service_requests as SR
import csv_structures.traffic_accidents as TA
import sql_checks
from argparse import ArgumentParser

parser = ArgumentParser()
parser.add_argument("-r", "--run", dest="run", action="store", help="test script number", required=True)
args = parser.parse_args()

force = True

service_request = models.DataSet(
    name='service_request',
    external='https://www.denvergov.org/media/gis/DataCatalog/311_service_requests/csv/311_service_requests.csv',
    struct=models.Struct(SR.COLUMNS, SR.KEY_COLUMNS, SR.UPSERT_COLUMNS, SR.PD_RAW, col_index=False),
    test='raw/test_files/service_request_' + args.run + '.csv'
)

traffic_accidents = models.DataSet(
    name='traffic_accidents',
    external='https://www.denvergov.org/media/gis/DataCatalog/traffic_accidents/csv/traffic_accidents.csv',
    struct=models.Struct(TA.COLUMNS, TA.KEY_COLUMNS, TA.UPSERT_COLUMNS, TA.PD_RAW, col_index=True),
    test='raw/test_files/traffic_accidents_' + args.run + '.csv'
)

service_request_download = utils.Download(
    name='service_request',
    base_model=service_request,
    force=force
)

traffic_accidents_download = utils.Download(
    name='traffic_accidents',
    base_model=traffic_accidents,
    force=force
)

data_sets = dict(
    traffic_accidents=traffic_accidents,
    service_request=service_request
)


updated_files = filter(
    None, [
        service_request_download.csv_request(),
        traffic_accidents_download.csv_request()
    ])

for structure_name in updated_files:
    load_data.load_csv(
        data_sets[structure_name],
        local_only=True)

if args.run == "2":
    sql_checks.check_all()

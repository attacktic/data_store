
```
$ docker build && docker compose up
```

## How does it work?

Docker calls data_loader/main.py -f [force all updates] -l [run local csv files only, forces by default].
It then:
1. Checks csv timestamp from url and returns the data_set name if the difference from current timestamp is greater than 10 min (interval in which script should run).
2. If an update is needed pandas will collect the csv file in chunks, cleanup the data, create a dataframe and insert into a stage postgresql db table.
3. Takes records from stage table and performs an insert or update to the production table with constraints.


## Tests

Tests can be run by replacing services.data_loader.build.dockerfile in docker-compose.yml from "Dockerfile" to "test_Dockerfile".

## Script Arguments

main.py:
```
  -f, --force              Force all updates
  -l, --local_only list    Run local csv files only, adds -f by default
```
These can be added or removed from data_loader/Dockerfile

test.py:
```
  -r, --run [1,2]          Test file number, use to run scripts subsequently
```

## Logging

Debugging and error logs can be found in data_loader:/var/log/application.log.
Some debugging statements will also show up in the console when running with the -l argument.

## DB Connection

```
$ docker exec -it [CONTAINER ID] psql -U docker
```

Sample queries are listed in: data_loader/sql/queries.sql

## Notes

Even though the data_loader service is set to wait for the postgres db container to initialize, it might take a few more seconds to warm up and may cause an error on the first run.

